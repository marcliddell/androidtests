package test.marc.com.test.ui.post

import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import androidx.databinding.DataBindingUtil
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.annotation.StringRes
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentStatePagerAdapter
import com.google.android.material.snackbar.Snackbar
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import test.marc.com.test.R
import test.marc.com.test.databinding.ActivityPostListBinding
import timber.log.Timber

class PostListActivity : AppCompatActivity() {

    private lateinit var binding: ActivityPostListBinding
    private lateinit var viewModel: PlacesListViewModel

    private var errorSnackbar: Snackbar? = null


    override fun onCreate(savedInstanceState: Bundle?){
        super.onCreate(savedInstanceState)

        binding = DataBindingUtil.setContentView(this, R.layout.activity_post_list)

        viewModel = ViewModelProviders.of(this).get(PlacesListViewModel::class.java)

        viewModel.errorMessage.observe(this, Observer {
            errorMessage -> if(errorMessage != null) showError(errorMessage) else hideError()
        })
        binding.viewModel = viewModel

        binding.mainViewpager.adapter = PlacesViewPagerAdapter(supportFragmentManager)
        binding.tabLayout.setupWithViewPager(binding.mainViewpager)
    }

    private fun showError(@StringRes errorMessage:Int){
        errorSnackbar = Snackbar.make(binding.root, errorMessage, Snackbar.LENGTH_INDEFINITE)
        errorSnackbar?.setAction(R.string.retry, viewModel.errorClickListener)
        errorSnackbar?.show()
    }

    private fun hideError(){
        errorSnackbar?.dismiss()
    }


    class PlacesViewPagerAdapter(fm: FragmentManager) : FragmentStatePagerAdapter(fm) {
        override fun getItem(position: Int): Fragment {
            return when(position){
                0 -> PlacesListFragment()
                1 -> PlacesListFragment()
                else -> PlacesListFragment()
            }
        }

        override fun getCount() = 2

        override fun getPageTitle(position: Int): CharSequence {
            return when (position) {
                0 -> "List"
                1 -> "Map"
                else -> "Unknown"
            }
        }
    }
}

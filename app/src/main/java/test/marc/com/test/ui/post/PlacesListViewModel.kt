package test.marc.com.test.ui.post

import androidx.lifecycle.MutableLiveData
import android.view.View
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import test.marc.com.test.R
import test.marc.com.test.base.BaseViewModel
import test.marc.com.test.model.PlaceResult
import test.marc.com.test.network.PlacesApi
import javax.inject.Inject


class PlacesListViewModel : BaseViewModel() {
    @Inject lateinit var placesApi: PlacesApi

    private lateinit var subscription: Disposable

    val loadingVisibility: MutableLiveData<Int> = MutableLiveData()
    val placesListAdapter: PlacesListAdapter = PlacesListAdapter()

    val errorMessage:MutableLiveData<Int> = MutableLiveData()
    val errorClickListener = View.OnClickListener { loadPosts() }

    init{
        loadPosts()
    }

    private fun loadPosts(){
        subscription = placesApi.getPlaces("51.4620979,-0.3111058", "bar")
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doOnSubscribe { onRetrievePostListStart() }
                .doOnTerminate { onRetrievePostListFinish() }
                .subscribe(
                        { result -> onRetrievePostListSuccess(result) },
                        { e-> onRetrievePostListError(e) }
                )
    }

    private fun onRetrievePostListStart(){
        loadingVisibility.value = View.VISIBLE
        errorMessage.value = null
    }

    private fun onRetrievePostListFinish(){
        loadingVisibility.value = View.GONE
    }

    private fun onRetrievePostListSuccess(postList:PlaceResult){
        placesListAdapter.updatePostList(postList)
    }

    private fun onRetrievePostListError(e : Throwable){
        errorMessage.value = R.string.post_error
        e.printStackTrace()
    }

    override fun onCleared() {
        super.onCleared()
        subscription.dispose()
    }
}
package test.marc.com.test.ui.post

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import test.marc.com.test.databinding.FragmentPlacesListBinding


class PlacesListFragment : Fragment() {

    private lateinit var viewModel: PlacesListViewModel

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {

        val binding = FragmentPlacesListBinding.inflate(inflater, container, false)
        viewModel = ViewModelProviders.of(this).get(PlacesListViewModel::class.java)

        binding.viewModel = viewModel
        binding.postList.layoutManager = LinearLayoutManager(context, RecyclerView.VERTICAL, false)

        return binding.root
    }


}
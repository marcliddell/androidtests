package test.marc.com.test.ui.post

import androidx.lifecycle.MutableLiveData
import test.marc.com.test.base.BaseViewModel
import test.marc.com.test.model.Place
import test.marc.com.test.model.Post


class PlacesViewModel: BaseViewModel() {
    private val postTitle = MutableLiveData<String>()
    private val postBody = MutableLiveData<String>()

    fun bind(place: Place){
        postTitle.value = place.name
        postBody.value = place.geometry.location.toString()
    }

    fun getPostTitle():MutableLiveData<String>{
        return postTitle
    }

    fun getPostBody():MutableLiveData<String>{
        return postBody
    }
}
package test.marc.com.test.model


data class PlaceResult (var results : List<Place>)

data class Place(val geometry : Geometry, val icon : String, val name : String)

data class Geometry(val location : Location)

data class Location(val lat : Double, val lng : Double)
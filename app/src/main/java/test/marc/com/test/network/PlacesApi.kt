package test.marc.com.test.network

import io.reactivex.Observable
import retrofit2.http.GET
import retrofit2.http.Query
import test.marc.com.test.model.PlaceResult


interface PlacesApi {
    /**
     * Get the list of the nearby places from the API
     */
    @GET("https://maps.googleapis.com/maps/api/place/nearbysearch/json?key=AIzaSyBAgwN6-AHgbOcr13r0z0wp7R7kLqJJGG0")
    fun getPlaces(@Query("location") latlng: String,
                 @Query("query") type: String,
                 @Query("radius") radius : Int = 1500): Observable<PlaceResult>
}
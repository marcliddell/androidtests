package test.marc.com.test.injection.component

import dagger.Component
import test.marc.com.test.module.NetworkModule
import test.marc.com.test.ui.post.PlacesListViewModel
import javax.inject.Singleton

@Singleton
@Component(modules = [(NetworkModule::class)])
interface ViewModelInjector {

    fun inject(postListViewModel: PlacesListViewModel)

    @Component.Builder
    interface Builder {
        fun build(): ViewModelInjector

        fun networkModule(networkModule: NetworkModule): Builder
    }
}
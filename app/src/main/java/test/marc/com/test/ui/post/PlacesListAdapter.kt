package test.marc.com.test.ui.post

import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import test.marc.com.test.R
import test.marc.com.test.databinding.ItemPostBinding
import test.marc.com.test.model.Place
import test.marc.com.test.model.PlaceResult


class PlacesListAdapter: RecyclerView.Adapter<PlacesListAdapter.ViewHolder>() {
    private lateinit var postList:List<Place>

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val binding: ItemPostBinding = DataBindingUtil.inflate(LayoutInflater.from(parent.context), R.layout.item_post, parent, false)
        return ViewHolder(binding)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bind(postList[position])
    }

    override fun getItemCount(): Int {
        return if(::postList.isInitialized) postList.size else 0
    }

    fun updatePostList(result:PlaceResult){
        this.postList = result.results
        notifyDataSetChanged()
    }

    class ViewHolder(private val binding: ItemPostBinding): RecyclerView.ViewHolder(binding.root){
        private val viewModel = PlacesViewModel()

        fun bind(place: Place){
            viewModel.bind(place)
            binding.viewModel = viewModel
        }
    }
}
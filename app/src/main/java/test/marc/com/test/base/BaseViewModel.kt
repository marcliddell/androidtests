package test.marc.com.test.base

import androidx.lifecycle.ViewModel
import test.marc.com.test.injection.component.DaggerViewModelInjector
import test.marc.com.test.injection.component.ViewModelInjector
import test.marc.com.test.module.NetworkModule
import test.marc.com.test.ui.post.PlacesListViewModel

abstract class BaseViewModel: ViewModel(){
    private val injector: ViewModelInjector = DaggerViewModelInjector
            .builder()
            .networkModule(NetworkModule)
            .build()

    init {
        inject()
    }

    /**
     * Injects the required dependencies
     */
    private fun inject() {
        when (this) {
            is PlacesListViewModel -> injector.inject(this)
        }
    }
}
